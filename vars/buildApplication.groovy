def call() {
    sh "mvn clean package -Pfatjar -DskipTests"
    stash(name: 'fatjar', includes: '**/target/fatjar.jar')
}
